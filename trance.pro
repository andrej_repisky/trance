TEMPLATE = app

QT += qml quick widgets sql webview

CONFIG += c++11

ios {
    QMAKE_INFO_PLIST = ios/Info.plist

    BUNDLE_DATA.files = $$files($$PWD/ios/Launch.xib) \
	$$files($$PWD/ios/*.png)

    QMAKE_BUNDLE_DATA += BUNDLE_DATA
}
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

QMAKE_CXXFLAGS += -include $$PWD/src/utils/Macros.h
RESOURCES += qml.qrc

include(deployment.pri)
android {
    include(android/android_openssl/openssl.pri)
}
win32 {
    QT -= webview
}

android {

    SOURCES += src/quazip/JlCompress.cpp \
	src/quazip/qioapi.cpp \
	src/quazip/quaadler32.cpp \
	src/quazip/quacrc32.cpp \
	src/quazip/quagzipfile.cpp \
	src/quazip/quaziodevice.cpp \
	src/quazip/quazip.cpp \
	src/quazip/quazipdir.cpp \
	src/quazip/quazipfile.cpp \
	src/quazip/quazipnewinfo.cpp \
	src/quazip/unzip.c \
	src/quazip/zip.c

    HEADERS += src/quazip/crypt.h \
	src/quazip/ioapi.h \
	src/quazip/JlCompress.h \
	src/quazip/quaadler32.h \
	src/quazip/quachecksum32.h \
	src/quazip/quacrc32.h \
	src/quazip/quagzipfile.h \
	src/quazip/quaziodevice.h \
	src/quazip/quazip.h \
	src/quazip/quazip_global.h \
	src/quazip/quazipdir.h \
	src/quazip/quazipfile.h \
	src/quazip/quazipfileinfo.h \
	src/quazip/quazipnewinfo.h \
	src/quazip/unzip.h \
	src/quazip/zip.h
}

SOURCES += \
    src/main.cpp \
    src/MainObject.cpp \
    src/navigation/TabEntry.cpp \
    src/navigation/TabModel.cpp \
    src/CServerCommunicator.cpp \
    src/PaintedImage.cpp \
    src/Downloader.cpp \
    src/FileStatus.cpp \
    src/utils/CustomSettings.cpp \
    src/utils/Settings.cpp \
    src/DownloadHistory.cpp \
    src/utils/NetworkCache.cpp

HEADERS += \
    src/MainObject.h \
    src/utils/Singleton.h \
    src/navigation/TabEntry.h \
    src/navigation/TabModel.h \
    src/CServerCommunicator.h \
    src/PaintedImage.h \
    src/Downloader.h \
    src/FileStatus.h \
    src/utils/CustomSettings.h \
    src/utils/Settings.h \
    src/utils/Macros.h \
    src/DownloadHistory.h \
    src/utils/NetworkCache.h

DISTFILES += \
    android/build.gradle \
    qml/main.qml \
    qml/navigation/ArrowTitleBar.qml \
    qml/navigation/MainTabView.qml \
    qml/navigation/MenuTitleBar.qml \
    qml/navigation/NavigationDrawer.qml \
    qml/navigation/PanelContent.qml \
    qml/navigation/TitleBar.qml \
    qml/navigation/TabPage.qml \
    qml/navigation/TitleBarButton.qml \
    qml/ebooks/EbooksPage.qml \
    qml/navigation/StackPage.qml \
    qml/settings/SettingsPage.qml \
    qml/controls/CSpinner.qml \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    qml/controls/CSeparator.qml \
    qml/controls/EmptyIndicator.qml \
    qml/ebooks/DetailPage.qml \
    qml/ebooks/InfoItem.qml \
    qml/controls/Toast.qml \
    qml/HelpPage.qml \
    qml/DownloadsPage.qml \
    qml/controls/CScrollBar.qml \
    qml/controls/BiggerLabel.qml \
    qml/ebooks/HistoryItem.qml \
    qml/ebooks/EbooksTitleBar.qml \
    qml/ebooks/WebViewPage.qml \
    qml/controls/formatUtils.js \
    qml/ebooks/RateEbookPage.qml
