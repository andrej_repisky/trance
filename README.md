# Mobilní aplikace Trance

Mobilní aplikace pro přístup do databáze autorů a publikací Trance. V binární formě je k dispozici v Google Play: https://play.google.com/store/apps/details?id=org.ondrejandrej.trance

### Návod na zbildování a spuštění na PC (Windows, Ubuntu)

Aplikace je napsaná multiplatformně v Qt. 

* Stáhni online instalátor z https://www.qt.io/download-qt-installer
* V instalátoru rozklikni šípku u zvolené verze Qt (aplikace vyžaduje aspoň 5.4, vhodná volba je 5.11) 
* Vyber binárky pro desktop (pod Ubuntu pro kompilátor "Desktop gcc 64-bit", pod Windows MinGW)
* Zbytek odklikej (Next)
* Otevři Qt Creator, File, Open File or Project, vyber soubor trance.pro v naklonovaném repozitáři trance.

PC verze aplikace sa spustí skratkou Ctrl-R, nebo zeleným trojúhelníkem vlevo dolu. Není nutný žádný emulátor Androidu.

### Spuštění pod Windows 

Pro fungování SSL je nutné stáhnout https://indy.fulgan.com/SSL/openssl-1.0.2r-i386-win32.zip a extrahovat `libeay32.dll` a `ssleay32.dll` do adresáře s trance.exe. Dále je nutné dočasně zakomentovat kód související s web view v souboru WebViewPage.qml.

