
#include "Settings.h"

#include <QDebug>
#include <QJsonDocument>
#include <QStandardPaths>
#include <QDir>

Settings::Settings(QObject *parent) :
    QObject(parent),
    m_settingsFile(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/settings.json")
{
    m_settingsFile.absoluteDir().mkpath(".");
    QFile jsonFile(m_settingsFile.filePath());
    if(!jsonFile.exists())
        save();
    if(!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qWarning() << "Failed to open settings file.";
    }
    QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonFile.readAll());
    m_settings = jsonDoc.toVariant().toMap();
}

void Settings::setValue(QString key, const QVariant &value) {
    if(m_settings.value(key) == value)
        return;
    m_settings.insert(key, value);
    save();
    emit valueChanged();
}

QVariant Settings::value(QString key, const QVariant &defaultVal) {
    if(!m_settings.contains(key) && defaultVal.isValid())
        setValue(key, defaultVal);
    return m_settings.value(key);
}

QVariant Settings::value(QString key) const
{
    return m_settings.value(key);
}

void Settings::save()
{
    QJsonDocument jsonDoc = QJsonDocument::fromVariant(m_settings);
    QFile jsonFile(m_settingsFile.filePath());
    if(!jsonFile.open(QIODevice::WriteOnly | QIODevice::Text))
        qWarning() << "Failed to open settings file for writing.";
    jsonFile.write(jsonDoc.toJson());
}

