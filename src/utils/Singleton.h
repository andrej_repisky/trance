
#pragma once

#include <QCoreApplication>

template<typename T>
class Singleton
{
public:
    static T &instance() {
        if(pInstance == nullptr)
            new T();
        return *pInstance;
    }

protected:
    Singleton() {
        Q_ASSERT_X(pInstance == nullptr, "Singleton", "There are multiple instances of a singleton class. "
                                                      "Make sure to use instance() method instead of calling constructor directly.");
        pInstance = static_cast<T *>(this);

        /* Following call ensures that destructor of Singleton and its subclasses is called
         * when the program terminates. It fixes the problem of gcc and MinGW, which do not
         * properly destroy heap-allocated objects. You may want to disable this code
         * with preprocessor directives if you compile this file in other compilers
         * or non-Qt environment.
         */
        QObject::connect(qApp, &QCoreApplication::aboutToQuit, [](){
            delete pInstance;
            pInstance = nullptr;
        });
    }
    ~Singleton() { }

private:
    static T *pInstance;
};  // class Singleton


template<typename T>
T* Singleton<T>::pInstance = nullptr;

