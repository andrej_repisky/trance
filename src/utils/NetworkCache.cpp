
#include "NetworkCache.h"
#include "CustomSettings.h"

#include <QFile>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardPaths>
#include <QDateTime>

#include <QtDebug>

const QString tableName = "network_cache";

NetworkCache::NetworkCache(QObject *parent) : QObject(parent)
{
    QString dbFilePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(dbFilePath);
    dir.mkpath(".");
    dbFilePath += "/cache.db";
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbFilePath);
    if(!db.open())
        qWarning() << "failed to open DB";
    QSqlQuery query;
    checkedExec(query, "PRAGMA foreign_keys = ON");
    checkedExec(query, "PRAGMA journal_mode = OFF");
    checkedExec(query, "PRAGMA synchronous = OFF");
    createSchema(query);
    removeOld(query);
}

QVariantList NetworkCache::model() const
{
    QSqlQuery query;
    checkedExec(query, "SELECT search_string FROM " + tableName + " ORDER BY date");
    QVariantList retVal;
    while(query.next()) {
        retVal.prepend(query.value(0));
    }
    return retVal;
}

void NetworkCache::insert(const QString &search_string, const QString &xml)
{
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO " + tableName + "(search_string, date, value) "
                  "VALUES (:search_string, :date, :value)");
    query.bindValue(":search_string", search_string);
    query.bindValue(":date", QDateTime::currentMSecsSinceEpoch() / 1000ll);
    query.bindValue(":value", xml);
    checkedExec(query);
    emit modelChanged();
}

QString NetworkCache::getValue(const QString &search_string) const
{
    QSqlQuery query;
    query.prepare("SELECT value FROM " + tableName + " WHERE search_string = :search_string");
    query.bindValue(":search_string", search_string);
    checkedExec(query);
    if(!query.next()) {
        qWarning() << "Medzipamäť neobsahuje reťazec " + search_string;
        return QString();
    }
    QSqlQuery updateQuery;
    const QString queryStr = QString("UPDATE %1 SET date = %2 WHERE search_string = '%3'")
            .arg(tableName)
            .arg(QDateTime::currentMSecsSinceEpoch() / 1000ll)
            .arg(search_string);
    checkedExec(updateQuery, queryStr);
    emit modelChanged();
    return query.value(0).toString();
}

void NetworkCache::createSchema(QSqlQuery &query)
{
    checkedExec(query, "CREATE TABLE IF NOT EXISTS " + tableName + " ( "
                        "search_string	TEXT	NOT NULL	PRIMARY KEY, "
                        "date	INTEGER	NOT NULL, "
                        "value TEXT   NOT NULL)");

    checkedExec(query, "CREATE INDEX IF NOT EXISTS idx_cache ON " + tableName + "(date, search_string)");
}

void NetworkCache::removeOld(QSqlQuery &query)
{
    const int maxCnt = CustomSettings::instance().value("maxSavedCnt", 40).toInt();
    const QString queryStr = QString("DELETE FROM %1 WHERE date IN (SELECT date FROM %1 ORDER BY date DESC LIMIT -1 OFFSET %2)")
            .arg(tableName)
            .arg(maxCnt);
    checkedExec(query, queryStr);
}

bool NetworkCache::checkedExec(QSqlQuery &query, const QString &sqlString) const
{
    bool result = sqlString.isEmpty() ? query.exec() : query.exec(sqlString);
    if(!result) {
        qWarning() << "query.exec() failed: " << query.lastError().text();
        qWarning() << "SQL: " << query.lastQuery();
    }
    return result;
}

