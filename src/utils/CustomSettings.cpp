
#include "CustomSettings.h"

#include <QDebug>
#include <QGuiApplication>
#include <QProcess>
#include <QJsonDocument>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>

CustomSettings::CustomSettings(QObject *parent) :
    Settings(parent) {}

QString CustomSettings::downloadPath() {
    return value("downloadPath", QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).toString();
}
