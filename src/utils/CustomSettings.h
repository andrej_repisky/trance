
#pragma once

#include "Settings.h"
#include "Singleton.h"

/*!
  \author Andrej Repiský
  \date   11. 12. 2015
  \brief  Nastavenia

  Trieda umožňuje uloženie nastavení s podobným rozhraním ako QVariantMap.
  */

class CustomSettings : public Settings, public Singleton<CustomSettings>
{
    Q_OBJECT

public:
    CustomSettings(QObject *parent = 0);

    Q_INVOKABLE QString downloadPath();
    QString ebooksPath() { return downloadPath() + "/trance_eknihy"; }
};


