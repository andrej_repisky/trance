
#pragma once

#include <QObject>
#include <QVariant>

class QSqlQuery;

class NetworkCache : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList model READ model NOTIFY modelChanged)

public:
    NetworkCache(QObject *parent = 0);
    QVariantList model() const;
    Q_INVOKABLE void insert(const QString &search_string, const QString &xml);
    Q_INVOKABLE QString getValue(const QString &search_string) const;

signals:
    void modelChanged() const;

private:
    bool checkedExec(QSqlQuery &query, const QString &sqlString = QString()) const;
    void createSchema(QSqlQuery &query);
    void removeOld(QSqlQuery &query);
};
