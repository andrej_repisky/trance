
#pragma once

#if defined(Q_OS_LINUX) || defined(Q_OS_ANDROID)
#include <QDebug>

#define PRINT_VAR(a)	qDebug() << #a ":" << a

#endif


