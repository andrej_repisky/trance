
#pragma once

#include <QObject>
#include <QJSValue>
#include <QFileInfo>
#include <QString>
#include <QVariant>

/*!

Trieda umožňuje uloženie nastavení s podobným rozhraním ako QVariantMap.
Pre správnu funkciu by mala v aplikácii existovať len 1 inštancia tejto triedy.

Ukážka použitia v QML - hodnoty sa aktualizujú, keď sa niekde zavolá settings.setValue():

    Label {
        text: settings.value("title" + settings.emptyString)
    }
*/

class Settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString emptyString READ getEmptyStr NOTIFY valueChanged)

public:
    Settings(QObject *parent = 0);

    Q_INVOKABLE QVariant value(QString key, const QVariant &defaultVal);    //!< Bacha: táto funkcia vytvorí položku s hodnotou defaultVal, pokiaľ položka neexistuje.
    Q_INVOKABLE QVariant value(QString key) const;
    void setValue(QString key, const QVariant &value);
    Q_INVOKABLE void setValue(QString key, const QJSValue &value) { setValue(key, value.toVariant()); }

    QString getEmptyStr() const { return QString(""); }

signals:
    void valueChanged();

private:
    void save();

    const QFileInfo m_settingsFile;
    QVariantMap m_settings;
};

