
#include "FileStatus.h"
#include "utils/CustomSettings.h"
#include "MainObject.h"
#ifdef Q_OS_ANDROID
#include "quazip/JlCompress.h"
#endif

#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QtDebug>
#include <QMessageBox>
#include <QTimer>

FileStatus::FileStatus(QObject *parent) :
	QObject(parent),
	m_settings(CustomSettings::instance())
{
    QTimer *updateTimer = new QTimer(this);
    updateTimer->setInterval(2000);
    connect(updateTimer, &QTimer::timeout, this, &FileStatus::update);
    updateTimer->start();
}

void FileStatus::setBookID(const QString &value)
{
	if(m_bookID == value)
		return;
	m_bookID = value;
	update();
}

QString FileStatus::getFileName(QString bookID)
{
	QDir downloadDir(CustomSettings::instance().downloadPath());
	if(!downloadDir.exists())
		return "";

	QStringList fileList = downloadDir.entryList(QStringList() << "*" + bookID + ".zip", QDir::Files);
	if(fileList.length() > 0)
		return fileList.at(0);
	else
		return "";
}

void FileStatus::update()
{
	m_archiveFile = QFileInfo();
	m_containedFile = QFileInfo();

	QString archiveFileName = getFileName(m_bookID);
	if(!archiveFileName.isEmpty())
		m_archiveFile = QFileInfo(m_settings.downloadPath() + "/" + archiveFileName);

#ifdef Q_OS_ANDROID
	if(m_archiveFile.exists()) {
		QStringList fileList = JlCompress::getFileList(m_archiveFile.absoluteFilePath());
		if(fileList.length() > 0) {
			m_containedFile = QFileInfo(m_settings.ebooksPath() + "/" + fileList.at(0));
		}
	}
#endif

    emit statusChanged();
}

FileStatus::Status FileStatus::status() const
{
	if(!m_archiveFile.exists())
		return Status::Absent;

	if(m_containedFile.fileName().isEmpty())
		return Status::Damaged;

	if(m_containedFile.exists())
		return Status::Extracted;

	return Status::Present;
}

bool FileStatus::extract() const
{
	QStringList extractedFiles;
#ifdef Q_OS_ANDROID
	extractedFiles = JlCompress::extractDir(m_archiveFile.absoluteFilePath(), m_settings.ebooksPath());
	emit statusChanged();
#endif
    const bool success = extractedFiles.length() > 0;
    if(!success)
        showReadOnlyError();
    return success;
}

void FileStatus::open() const
{
	QString file = "file:///" + m_containedFile.absoluteFilePath();
    QDesktopServices::openUrl(QUrl(file));
}

void FileStatus::erase()
{
    auto reply = QMessageBox::question(nullptr, "Smazat",
                          "Opravdu chceš smazat soubor?",
                          QMessageBox::Yes | QMessageBox::No);
    if(reply != QMessageBox::Yes)
        return;

    bool success = true;
    if(m_containedFile.exists())
        success = QFile::remove(m_containedFile.absoluteFilePath());
    if(m_archiveFile.exists())
        success = QFile::remove(m_archiveFile.absoluteFilePath());

    if(!success)
        showReadOnlyError();

    update();
}

void FileStatus::showReadOnlyError() const
{
	MainObject::instance().showToast("Chyba: není možný zápis do lokality "
									 + m_archiveFile.absoluteDir().absolutePath());
}

