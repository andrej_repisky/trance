#pragma once

#include <QObject>

class TabEntry : public QObject
{
    Q_OBJECT
	Q_PROPERTY(int index READ index WRITE setIndex)
	Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)
	Q_PROPERTY(QString title READ title WRITE setTitle)

public:
	TabEntry(QObject *parent = 0);
	~TabEntry();
	int index() const { return m_index; }
	bool selected() const { return m_selected; }
	QString title() const { return m_title; }
	void setIndex(const int value);
	void setSelected(const bool value);
	void setTitle(const QString value) { m_title = value; }

signals:
	void selectedChanged();

private:
	int m_index;
	bool m_selected;
	QString m_title;
};

