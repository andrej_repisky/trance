
#include "TabEntry.h"
#include "TabModel.h"


TabEntry::TabEntry(QObject *parent):
	QObject(parent), m_index(-1), m_selected(false)
{}

TabEntry::~TabEntry() {
	TabModel::instance().removeTab(this);
}

void TabEntry::setIndex(const int value)
{
	if(m_index != value) {
		m_index = value;
		TabModel::instance().removeTab(this);
		TabModel::instance().addTab(this);
	}
}

void TabEntry::setSelected(const bool value)
{
	if(m_selected != value) {
		m_selected = value;
		emit selectedChanged();
	}
}

