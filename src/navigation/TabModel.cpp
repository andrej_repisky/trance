
#include "TabModel.h"
#include "TabEntry.h"
#include <QDebug>


QVariant TabModel::data(const QModelIndex &index, int role) const
{
	int itemIndex = index.row();
	if(!m_content.keys().contains(itemIndex)) {
		if(!m_content.isEmpty())
			qWarning() << "TabModel: no such index: " << itemIndex;
		return QVariant("");
	}
	switch(static_cast<Role>(role)) {
	case Role::Selected:
		return m_content.value(itemIndex)->selected();
		break;
	case Role::Title:
		return m_content.value(itemIndex)->title();
		break;
	default:
		return QVariant();
	}
}

void TabModel::addTab(TabEntry * const newEntry)
{
	emit layoutAboutToBeChanged();
	m_content.insert(newEntry->index(), newEntry);
	connect(newEntry, &TabEntry::selectedChanged,
			this, &TabModel::onSelectedChanged, Qt::UniqueConnection);
	emit layoutChanged();
}

void TabModel::removeTab(const TabEntry *newEntry)
{
	if(m_content.value(newEntry->index()) == newEntry) {
		emit layoutAboutToBeChanged();
		m_content.remove(newEntry->index());
		emit layoutChanged();
	}
}

void TabModel::onSelectedChanged()
{
	auto tabEntry = qobject_cast<TabEntry *>(sender());
	int index = m_content.key(tabEntry);
	auto modelIndex = QAbstractItemModel::createIndex(index, 0);
	emit dataChanged(modelIndex, modelIndex, { (int)Role::Selected });
}

QHash<int, QByteArray> TabModel::roleNames() const
{
	QHash<int, QByteArray> retVal;
	retVal.insert((int)Role::Selected, "selected");
	retVal.insert((int)Role::Title, "title");
	return retVal;
}

int TabModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	int maxIndex = 0;
	foreach(const int key, m_content.keys()) {
		if(key > maxIndex)
			maxIndex = key;
	}
	return maxIndex + 1;
}


