#pragma once

#include <QAbstractListModel>
#include "../utils/Singleton.h"

class TabEntry;

class TabModel : public QAbstractListModel, public Singleton<TabModel>
{
public:
	QHash<int, QByteArray> roleNames() const;
	int rowCount(const QModelIndex & parent = QModelIndex()) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

	void addTab(TabEntry * const newEntry);
	void removeTab(TabEntry const * newEntry);

private slots:
	void onSelectedChanged();

private:
	enum class Role {
		Selected = 0,
		Title
	};

	QMap<int, TabEntry *> m_content;
};

