
#include "DownloadHistory.h"
#include "utils/CustomSettings.h"

const QString ebookListKey = "downloadedEbooks";
const QString bookIDKey = "id";


DownloadHistory::DownloadHistory(QObject *parent) :
	QObject(parent),
	m_settings(CustomSettings::instance())
{

}

QVariantList DownloadHistory::downloadList() const
{
	return m_settings.value(ebookListKey, QVariantList()).toList();
}

void DownloadHistory::add(const QVariantMap &vMap)
{
	QVariantList vList = m_settings.value(ebookListKey).toList();
	for(int i = 0; i < vList.length(); i ++) {
		const QVariantMap ebookMap = vList.at(i).toMap();
		if(ebookMap.value(bookIDKey) == vMap.value(bookIDKey)) {
			vList.removeAt(i);
			break;
		}
	}
	vList.prepend(vMap);
	vList = vList.mid(0, 100);
	m_settings.setValue(ebookListKey, vList);
	emit downloadListChanged();
}
