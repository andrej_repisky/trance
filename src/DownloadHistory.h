#pragma once

#include <QObject>
#include <QVariant>

class CustomSettings;

class DownloadHistory : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QVariantList downloadList READ downloadList NOTIFY downloadListChanged)

public:
	explicit DownloadHistory(QObject *parent = nullptr);

	QVariantList downloadList() const;
	Q_INVOKABLE void add(const QVariantMap &vMap);

signals:
	void downloadListChanged();

private:
	CustomSettings &m_settings;

};
