
#pragma once

#include <QObject>
#include "utils/Singleton.h"

class QQmlApplicationEngine;
class QQuickItem;

class MainObject : public QObject, public Singleton<MainObject>
{
    Q_OBJECT
public:
	MainObject(QObject *parent = 0);

	Q_INVOKABLE void clearImageCache();

	Q_INVOKABLE void showToast(const QString &text);
    Q_INVOKABLE QString buildDate() const;

private:	
	void initQML();
	QQuickItem *rootItem();

	QQmlApplicationEngine *m_engine;
};

