
#pragma once

#include <QObject>
#include <QUrlQuery>
#include <QVariantMap>

class QNetworkAccessManager;
class QNetworkReply;

class CServerCommunicator : public QObject
{
    Q_OBJECT

	Q_PROPERTY(bool active READ active NOTIFY activeChanged)
	Q_PROPERTY(bool signedIn READ signedIn NOTIFY signedInChanged)

public:
    CServerCommunicator(bool showErros = true, QObject *parent = nullptr);

	bool active() const { return m_reply != nullptr; }
    bool signedIn() const { return !m_username.isEmpty() && !m_password.isEmpty(); }

    Q_INVOKABLE void setUsernamePassword(QString name, QString pwd);
    Q_INVOKABLE void send(const QString &apiCommand, QVariantMap values);

signals:
	void activeChanged();
	void signedInChanged();
    void responseReceived(const QVariant &data);
	void redirection(const QString &redirUrl);

private slots:
    void onReplyFinished();

private:
	bool isError();
	void httpPost(QString url, QUrlQuery postData = QUrlQuery());
    void showError(QString title, QString text) const;

    const QString baseURL = "https://xtrance.info/new/api.php";
//    const QString baseURL = "http://www.ulovok.info/klavesnica/stiahnut.php";

	QNetworkAccessManager *netManager();
    static QString m_username;
    static QString m_password;
    QNetworkReply *m_reply;
    bool mShowErrors;
};

