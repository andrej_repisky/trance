
#include "MainObject.h"
#include <QApplication>
#ifndef Q_OS_WIN
#include <QtWebView/QtWebView>
#endif
#include <QSslSocket>
#include <QDebug>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
    qDebug() << "Podpora OpenSSL: " << QSslSocket::supportsSsl();
#ifndef Q_OS_WIN
	QtWebView::initialize();
#endif

	MainObject mainObj;

    return app.exec();
}
