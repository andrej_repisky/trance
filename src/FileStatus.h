
#pragma once

#include <QObject>
#include <QFileInfo>

class CustomSettings;

class FileStatus : public QObject
{
	Q_OBJECT
	Q_ENUMS(Status)

	Q_PROPERTY(QString bookID READ bookID WRITE setBookID)
	Q_PROPERTY(Status status READ status NOTIFY statusChanged)

public:
	enum Status {
		Absent,
		Damaged,
		Present,
		Extracted
	};

    FileStatus(QObject *parent = nullptr);

	QString bookID() const { return m_bookID; }
	void setBookID(const QString &value);

	Status status() const;

	Q_INVOKABLE bool extract() const;
	Q_INVOKABLE void open() const;
    Q_INVOKABLE void erase();
	Q_INVOKABLE void update();


signals:
	void statusChanged() const;

private:
	static QString getFileName(QString bookID);
    void showReadOnlyError() const;

	QString m_bookID;
	QFileInfo m_archiveFile;
	QFileInfo m_containedFile;
	CustomSettings &m_settings;
};

