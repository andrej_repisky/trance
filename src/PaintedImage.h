
#pragma once

#include <QQuickPaintedItem>
#include <QImage>
#include <QMap>
#include "CServerCommunicator.h"

class QNetworkReply;

class PaintedImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString source READ source WRITE setSource)

public:
    PaintedImage(QQuickItem *parent = nullptr);
    void paint(QPainter *painter);
    QString source() const { return m_source; }
    void setSource(QString value);

    static void clearCache() { m_cache.clear(); }

private slots:
    void onResponseReceived(const QVariant &data);

private:
    static QMap<QString, QByteArray> m_cache;
    CServerCommunicator m_communicator;
    QImage m_image;
    QString m_source;
};
