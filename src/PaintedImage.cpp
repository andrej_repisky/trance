
#include "PaintedImage.h"
#include "CServerCommunicator.h"

#include <QBrush>
#include <QPainter>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

QMap<QString, QByteArray> PaintedImage::m_cache;

PaintedImage::PaintedImage(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_communicator(false)
{
    QImage image(30, 30, QImage::Format_RGB32);
    image.fill(QColor(qRgb(230, 230, 230)));
    m_image = image;

    connect(&m_communicator, &CServerCommunicator::responseReceived, this, &PaintedImage::onResponseReceived);
}

void PaintedImage::paint(QPainter *painter)
{
    painter->drawImage(contentsBoundingRect(), m_image);
}

void PaintedImage::setSource(QString value)
{
    if(m_source == value)
        return;
    m_source = value;

    if(m_cache.contains(m_source)) {
        QImage returnImage;
        if(returnImage.loadFromData(m_cache.value(m_source), "JPG")) {
            m_image = returnImage;
            update();
        }
        return;
    }
    QVariantMap outData;
    outData.insert("itemId", m_source);
    m_communicator.send("getPublicationImageBase64", outData);
}

void PaintedImage::onResponseReceived(const QVariant &data)
{
    const QByteArray stringBase64 = data.toMap().value("data").toByteArray();
    const QByteArray imgData = QByteArray::fromBase64(stringBase64);

    QImage returnImage;
    if(returnImage.loadFromData(imgData, "JPG")) {
        if(!m_cache.contains(m_source))
            m_cache.insert(m_source, imgData);
        m_image = returnImage;
        update();
    }
}

