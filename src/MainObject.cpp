
#include "MainObject.h"
#include "navigation/TabEntry.h"
#include "navigation/TabModel.h"
#include "CServerCommunicator.h"
#include "utils/CustomSettings.h"
#include "utils/NetworkCache.h"
#include "PaintedImage.h"
#include "Downloader.h"
#include "DownloadHistory.h"
#include "FileStatus.h"

#include <QtQml>
#include <QApplication>
#include <QScreen>
#include <QDebug>
#include <QQuickWindow>
#include <QQuickItem>
#include <QQmlComponent>
#include <QTimer>

#define REGISTER_QML(a) qmlRegisterType<a>("custom", 1, 0, #a);

MainObject::MainObject(QObject *parent) : QObject(parent)
{
    initQML();
}

void MainObject::initQML()
{
    REGISTER_QML(TabEntry)
    REGISTER_QML(CServerCommunicator)
    REGISTER_QML(PaintedImage)
    REGISTER_QML(FileStatus)
    REGISTER_QML(NetworkCache)

    m_engine = new QQmlApplicationEngine(this);
    QQmlContext *context = m_engine->rootContext();

    qreal pxPermm = qApp->primaryScreen()->physicalDotsPerInch() / 25.4;
    if(qApp->primaryScreen()->physicalSize().height() > 120.0)	// iPad etc.
    {
        pxPermm = pxPermm * 1.3;
    }
#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
    pxPermm = 6;
#endif

    context->setContextProperty("mm", pxPermm);

    context->setContextProperty("_tabModel", &TabModel::instance());
    context->setContextProperty("_fgColor", "#606090");
    context->setContextProperty("_settings", &CustomSettings::instance());
    context->setContextProperty("_downloader", &Downloader::instance());
    context->setContextProperty("_downloadHistory", new DownloadHistory(this));
    context->setContextProperty("_mainObject", this);
#ifdef Q_OS_IOS
    context->setContextProperty("_isIOS", QVariant(true));
#else
    context->setContextProperty("_isIOS", QVariant(false));
#endif
    m_engine->load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
}

QQuickItem *MainObject::rootItem()
{
    if(m_engine->rootObjects().isEmpty())
        return nullptr;
    QQuickWindow *window = qobject_cast<QQuickWindow *>(m_engine->rootObjects().first());
    QQuickItem *topItem = window->contentItem();

    while(topItem->parentItem())
        topItem = topItem->parentItem();

    return topItem;
}

void MainObject::clearImageCache()
{
    PaintedImage::clearCache();
}

void MainObject::showToast(const QString &text)
{
    if(text.trimmed().isEmpty())
        return;
    QQmlComponent component(m_engine, QUrl("qrc:/qml/controls/Toast.qml"));
    QQuickItem *toast = qobject_cast<QQuickItem*>(component.create());
    toast->setParentItem(rootItem());
    toast->setParent(this);
    toast->setProperty("text", text);
    QTimer *timer = new QTimer(toast);
    timer->setInterval(5000);
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, toast, &QQuickItem::deleteLater);
    timer->start();
}

QString MainObject::buildDate() const
{
    QLocale english(QLocale::English);

    QString dateStr = QStringLiteral(__DATE__);
    const QDateTime date = english.toDateTime(dateStr.simplified(), "MMM d yyyy");
    return date.toString("d. M. yyyy");
}
