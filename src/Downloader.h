
#pragma once

#include <QObject>
#include "utils/Singleton.h"

class QNetworkAccessManager;
class QNetworkReply;
class QFile;

class Downloader : public QObject, public Singleton<Downloader>
{
	Q_OBJECT

	Q_PROPERTY(int downloadedSize READ downloadedSize NOTIFY downloadedSizeChanged)
	Q_PROPERTY(QString bookID READ bookID NOTIFY runningChanged)
public:
	Downloader(QObject *parent = 0);

	Q_INVOKABLE void downloadZip(const QString bookID, const QUrl &zipUrl);
	QString bookID() const { return m_bookID; }
	int downloadedSize() const { return m_downloadedSize; }

signals:
	void runningChanged(bool value);
	void downloadedSizeChanged();

private:
	void onDownloadZipFinished(QNetworkReply *reply);

	QNetworkAccessManager *m_netManager;
	QNetworkReply *m_reply;
	QString m_bookID;
	int m_downloadedSize;
};

