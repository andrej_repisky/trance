
#include "CServerCommunicator.h"
#include "utils/CustomSettings.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QMessageBox>
#include <QElapsedTimer>

#define SHOWDEBUG

#if !defined(QT_DEBUG) || defined(Q_OS_ANDROID)
#undef SHOWDEBUG
#endif

QString CServerCommunicator::m_username;
QString CServerCommunicator::m_password;

CServerCommunicator::CServerCommunicator(bool showErros, QObject *parent):
    QObject(parent)
{
    m_reply = nullptr;
    mShowErrors = showErros;
}

void CServerCommunicator::setUsernamePassword(QString name, QString pwd)
{
    m_username = name;
    m_password = pwd;
    emit signedInChanged();
}

void CServerCommunicator::send(const QString &apiCommand, QVariantMap values)
{
    if(m_reply != nullptr)
        return;

    if(!signedIn()) {
        showError("Nepřihlášený uživatel", "Vyplň svoje přihlašovací údaje v Nastavení.");
        return;
    }
    values.insert("userName", m_username);
    values.insert("userPassword", m_password);
    const QJsonDocument json = QJsonDocument::fromVariant(values);
    const QString jsonStr = QString::fromUtf8(json.toJson());
#ifdef SHOWDEBUG
    qDebug() << apiCommand << ": " << jsonStr;
#endif
    QUrlQuery data;
    data.addQueryItem("data", jsonStr);
    httpPost(baseURL + "?command=" + apiCommand, data);
    connect(m_reply, &QNetworkReply::finished, this, &CServerCommunicator::onReplyFinished);
}

void CServerCommunicator::onReplyFinished()
{
    if(isError())
        return;

    QByteArray jsonArray = m_reply->readAll();
    m_reply->deleteLater();
    m_reply = nullptr;
    emit activeChanged();

#ifdef SHOWDEBUG
    qDebug() << __PRETTY_FUNCTION__ << jsonArray.left(150);
#endif
    const QJsonDocument document = QJsonDocument::fromJson(jsonArray);
    if(document.isNull()) {
        showError("Chyba", "Neplatný formát odpovědi. Text: " + QString::fromUtf8(jsonArray));
        return;
    }
    const QVariant variant = document.toVariant();
    const QVariantMap vMap = variant.toMap();
    if(vMap.contains("errNumber")) {
        QString errorNr = vMap.value("errNumber").toString();
        showError("Chyba č. " + errorNr, vMap.value("errText").toString());
        if(errorNr.toInt() == 1) {  // nesprávne prihl. údaje
            m_username.clear();
            m_password.clear();
            emit signedInChanged();
        }
        return;
    }
    emit responseReceived(variant);
}

bool CServerCommunicator::isError()
{
    bool retVal = (m_reply->error() != QNetworkReply::NoError);
    if(retVal) {
#ifndef QT_DEBUG
        QString errorMsg = "Nepodařilo se spojit se serverem. Zkontroluj připojení k internetu.";
#else
        QString errorMsg = QString("Error %1: %2").arg(m_reply->error()).arg(m_reply->errorString());
#endif
        showError("Chyba", errorMsg);
        m_reply->deleteLater();
        m_reply = nullptr;
        emit activeChanged();
    }
    return retVal;
}

void CServerCommunicator::httpPost(QString url, QUrlQuery postData)
{
    QNetworkRequest request{QUrl(url)};
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded; charset=UTF-8");
    m_reply = netManager()->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    emit activeChanged();
}

void CServerCommunicator::showError(QString title, QString text) const
{
    if(mShowErrors)
        QMessageBox::critical(nullptr, title, text);
}

QNetworkAccessManager *CServerCommunicator::netManager()
{
    static QNetworkAccessManager netManager;
    return &netManager;
}
