
#include "Downloader.h"
#include "FileStatus.h"
#include "utils/CustomSettings.h"

#include <QDebug>
#include <QMessageBox>

#include <QDesktopServices>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>

Downloader::Downloader(QObject *parent) :
	QObject(parent),
	m_netManager(new QNetworkAccessManager(this)),
	m_reply(nullptr),
	m_downloadedSize(0)
{
	m_netManager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
}

void Downloader::downloadZip(const QString bookID, const QUrl &zipUrl)
{
	if(m_reply != nullptr)
		return;
	connect(m_netManager, &QNetworkAccessManager::finished,
			this, &Downloader::onDownloadZipFinished, Qt::UniqueConnection);
	m_reply = m_netManager->get(QNetworkRequest(QUrl(zipUrl)));
	m_bookID = bookID;
	m_downloadedSize = 0;
	connect(m_reply, &QNetworkReply::downloadProgress, [this](int value) {
		m_downloadedSize = value;
		emit downloadedSizeChanged();
	});
	emit downloadedSizeChanged();
	emit runningChanged(true);
}

void Downloader::onDownloadZipFinished(QNetworkReply *reply)
{
	if(reply != m_reply) {
		return;
	}
	if(m_reply->error() != QNetworkReply::NoError) {
		qDebug() << m_reply->errorString();
		m_reply->deleteLater();
		m_reply = nullptr;
		return;
	}

	QByteArray data = m_reply->readAll();
	const QString fileName = m_reply->url().toString().split("/").last();
	m_reply->deleteLater();
	m_reply = nullptr;


	QFile file(CustomSettings::instance().downloadPath() + "/" + fileName);
	file.open(QIODevice::WriteOnly);
	file.write(data);
	file.close();
	m_bookID.clear();
	emit runningChanged(false);
	qDebug() << data.length();
}
