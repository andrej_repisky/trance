
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import custom 1.0
import "navigation"
import "controls"
import "ebooks"
import "controls/formatUtils.js" as FormatUtils

TabPage {
	title: "Stažené soubory"
	Item {
		ListView {
			anchors.fill: parent
			id: listView
			clip: true
			model: _downloadHistory.downloadList
			delegate: Rectangle {
				id: delegateItem
				color: "white"
				height: 14 * mm
				width: listView.width
				clip: true
				RowLayout {
					anchors.fill: parent
					anchors.margins: mm
					Item {
						id: textItem
                        height: parent.height
						clip: true
						MouseArea {
							id: mouseArea
							anchors.fill: parent
							onClicked: {
								var page = detailPageComponent.createObject()
								page.ebookData = modelData
								stackView.push(page)
							}
						}
						Label {
							anchors.left: parent.left
							anchors.right: parent.right
							anchors.top: parent.top
							text: modelData.title
							font.bold: true
                            textFormat: Text.StyledText
							elide: Text.ElideRight
						}
						Label {
							anchors.left: parent.left
							anchors.verticalCenter: parent.verticalCenter
							text: modelData.format
						}
						Label {
							id: fileLabel
							anchors.bottom: parent.bottom
							anchors.left: parent.left
                            text: "*" + modelData.id + ".zip"
						}
						Layout.fillWidth: true
					}
					Button {
                        Layout.alignment: Qt.AlignVCenter
						visible: (fileStatus.status !== FileStatus.Absent) && !statusIndicator.isDownloading
						width: 15 * mm
						text: "Smazat"
						onClicked:
							fileStatus.erase()
					}
					Rectangle {
						id: statusIndicator
						visible: !_isIOS
						width: 15 * mm
                        height: parent.height
                        property bool isDownloading: (_downloader.bookID === modelData.id)
						onIsDownloadingChanged: {
							if(!isDownloading)
								fileStatus.update()
						}
						Label {
							anchors.verticalCenter: parent.verticalCenter
							anchors.left: parent.left
							anchors.right: parent.right
							visible: fileStatus.status === FileStatus.Absent && !statusIndicator.isDownloading
							text: "Soubor neexistuje"
							color: "grey"
							font.italic: true
							horizontalAlignment: Text.AlignHCenter
							wrapMode: Text.WordWrap
						}
                        Label {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                            visible: fileStatus.status === FileStatus.Damaged && !statusIndicator.isDownloading
                            text: "Archiv je poškozen"
                            color: "grey"
                            font.italic: true
                            horizontalAlignment: Text.AlignHCenter
                            wrapMode: Text.WordWrap
                        }
						Column {
							anchors.verticalCenter: parent.verticalCenter
							anchors.left: parent.left
							anchors.right: parent.right
							visible:  statusIndicator.isDownloading
							Label {
								anchors.left: parent.left
								anchors.right: parent.right
                                text: FormatUtils.formatSize(_downloader.downloadedSize) + "/" + FormatUtils.formatSize(modelData.size)
                                //color: "grey"
                                //font.italic: true
								horizontalAlignment: Text.AlignHCenter
							}
							ProgressBar {
								anchors.left: parent.left
								anchors.right: parent.right
								anchors.margins: mm
								value: _downloader.downloadedSize / modelData.size
							}
						}

						Button {
							anchors.verticalCenter: parent.verticalCenter
							anchors.left: parent.left
							anchors.right: parent.right
							visible: fileStatus.status === FileStatus.Present && !statusIndicator.isDownloading
							text: "Rozbalit"
							onClicked: {
								if(fileStatus.extract())
									_mainObject.showToast("Rozbalení proběhlo úspěšně.")
							}
						}
						Button {
							anchors.verticalCenter: parent.verticalCenter
							anchors.left: parent.left
							anchors.right: parent.right
							visible: fileStatus.status === FileStatus.Extracted && !statusIndicator.isDownloading
							text: "Otevřít"
							onClicked: {
								fileStatus.open()
							}
						}
					} // Rectangle
					FileStatus {
						id: fileStatus
                        bookID: modelData.id
					}
				}
				Rectangle {
					anchors.fill: parent
					color: "black"
					opacity: 0.1
					visible: mouseArea.pressed
				}
				CSeparator {
					anchors.bottom: parent.bottom
				}
			}	// delegate item
			CScrollBar {
			}
			Component {
				id: detailPageComponent
				DetailPage {
				}
			}
		}	// ListView
		EmptyIndicator {
			visible: listView.count == 0
			text: "Žádné položky"
		}
	}	// Item
}
