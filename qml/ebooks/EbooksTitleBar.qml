
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import "../navigation"

MenuTitleBar {
    id: titleBar
    text: {
        if(lastSearch !== "")
            return "Hledání „" + lastSearch + "“"
        else
            return rootTab.title
    }

    property bool searchOpen: true
    property alias searchText: searchField.text
    property string lastSearch: ""
    onSearchOpenChanged: {
        if(searchOpen)
            searchField.forceActiveFocus()
        else
            searchField.focus = false
    }

    states: State {
        name: "searchOpen"
        when: titleBar.searchOpen
        AnchorChanges {
            target: searchBar
            anchors.top: titleBar.bottom
            anchors.bottom: undefined
        }
    }
    transitions: Transition {
        AnchorAnimation { duration: 100; easing.type: Easing.OutCubic }
    }
    onClicked:
        drawer.toggle()
    Connections {
        target: drawer
        onOpenChanged: {
            if(drawer.open)
                titleBar.searchOpen = false
        }
    }
    rightContent: TitleBarButton {
        Image {
            anchors.centerIn: parent
            height:  parent.height / 2
            width: parent.height / 2
            source: "qrc:/resources/lupa.png"
            fillMode: Image.PreserveAspectFit
            clip: true
        }
        onClicked: {
            if(drawer.open)
                return
            titleBar.searchOpen = !titleBar.searchOpen
        }
    }
    z: 2
    function runSearch() {
        var searchTerm = searchField.text
        if(searchTerm.trim() === "")
            return

        titleBar.searchOpen = false
        var sendData = {
            "itemCount": 1000,
            "filter": searchTerm
        }
        serverComm.send("getEbookList", sendData)
    }
    Rectangle {
        id: searchBar
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 8 * mm
        color: "#D0D0E0"
        z: -1
        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 2 * mm
            anchors.rightMargin: mm
            TextField {
                id: searchField
                Layout.alignment: Qt.AlignVCenter
                placeholderText: "Hledaný text"
                text: (Qt.platform.os === "linux") ? "Hesse" : ""
                onAccepted:
                    titleBar.runSearch()
                focus: true
                Layout.fillWidth: true
                Keys.onReleased: {
                    if(event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
                        titleBar.searchOpen = false
                        event.accepted = true
                    }
                }
            }
            Button {
                id: searchButton
                text: "Hledat"
                width: 14 * mm
                onClicked: {
                    titleBar.runSearch()
                }
            }
        }
    }

}
