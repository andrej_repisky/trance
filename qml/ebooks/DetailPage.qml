
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import custom 1.0
import "../navigation"
import "../controls"
import "../controls/formatUtils.js" as FormatUtils

StackPage {
    id: rootPage
    property variant ebookData

    onEbookDataChanged: {
        var data = rootPage.ebookData
        image.source = data.publicationId
        title.text = data.title
        rootPage.title = data.title
        pub_info.text = data.publicationInfo
        publishers.text = data.publishers + " (" + data.releaseYear + ")"
        authors.text = data.authors
        original_language.text = data.originalLanguage
        rating.text = data.ebookRating + " % / " + data.publicationRating + " %"
        format.text = data.format
        sizeLabel.text = FormatUtils.formatSize(data.size)
        statusLabel.text = data.ebookStatus
        downloadRepeater.model = data.downloadLinks
    }
    titleBarMenuContent: TitleBarButton {
        Image {
            anchors.centerIn: parent
            height:  parent.height / 2
            width: parent.height / 2
            source: "qrc:/resources/hvezda.png"
            fillMode: Image.PreserveAspectFit
            clip: true
        }
        onClicked: {
            var page = ratePageComponent.createObject()
            page.ebookData = rootPage.ebookData
            stackView.push(page)
        }
        Component {
            id: ratePageComponent
            RateEbookPage {
            }
        }
    }

    CServerCommunicator {
        id: serverComm
        onResponseReceived: {
            var redirUrl = data["url"]
            var urlSplit = redirUrl.split("/")
            var domain = urlSplit[2]

            if(domain.indexOf("zippyshare.com") > -1) {
                var page = dlnPageComponent.createObject()
                page.downloadLink = redirUrl
                stackView.push(page)
            }
            else {
                Qt.openUrlExternally(redirUrl)
            }
        }
        onActiveChanged:
            spinner.visible = active
    }
    Component {
        id: dlnPageComponent
        WebViewPage {
            onZipLinkReceived: {
                if(_downloader.bookID === ebookData.bookID)
                    return
                if(_downloader.bookID !== "") {
                    close()
                    _mainObject.showToast("Odkaz nejde otevřít. Probíhá stahování jiné knihy.")
                    return
                }
                tabView.currentIndex = 1
                while(stackView.depth > 1)
                    stackView.currentItem.close()
                _downloader.downloadZip(ebookData.bookID, zipLink)
            }
        }
    }
    Flickable {
        id: flickable
        anchors.fill: parent
        topMargin: mm
        bottomMargin: mm
        clip: true
        flickableDirection: Flickable.VerticalFlick
        contentHeight: flickItem.height
        Item {
            id: flickItem
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: mm
            height: childrenRect.height
            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: mm
                Item {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: childrenRect.height
                    PaintedImage {
                        id: image
                        anchors.top: parent.top
                        anchors.left: parent.left
                        height: width * 1.5
                        width: appWindow.width * 0.43
                    }
                    Column {
                        anchors.top: parent.top
                        anchors.left: image.right
                        anchors.right: parent.right
                        anchors.leftMargin: 2 * mm
                        spacing: 2 * mm
                        InfoItem {
                            id: title
                            caption: "Název:"
                        }
                        InfoItem {
                            id: authors
                            caption: "Autoři:"
                        }
                        InfoItem {
                            id: publishers
                            caption: "Nakladatel (rok vydání):"
                        }
                        InfoItem {
                            id: original_language
                            visible: text !==  ""
                            caption: "Jazyk originálu:"
                        }
                        InfoItem {
                            id: rating
                            caption: "Hodnocení e-knihy/publikace:"
                        }
                        Item {
                            width: height
                            height: mm
                        }
                    }
                }   // Item
                Label {
                    id: pub_info
                    anchors.left: parent.left
                    anchors.right: parent.right
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                }
                CSeparator {
                }
                GridLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    columns: 2
                    Label {
                        text: "Formát: "
                        color: "grey"
                    }
                    Label {
                        id: format
                    }
                    Label {
                        text: "Velikost: "
                        color: "grey"
                    }
                    Label {
                        id: sizeLabel
                    }
                    Label {
                        text: "Stav: "
                        color: "grey"
                    }
                    Label {
                        id: statusLabel
                        elide: Text.ElideRight
                        Layout.fillWidth: true
                    }
                }
                GridLayout {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    columns: 2
                    Repeater {
                        id: downloadRepeater
                        delegate: Button {
                            text: "Server č. " + (index + 1)
                            onClicked: {
                                _downloadHistory.add(rootPage.ebookData)
                                var outData = {
                                    "itemId": rootPage.ebookData.id,
                                    "serverId": modelData
                                }
                                serverComm.send("getEbookUrl", outData)
                            }
                            Layout.fillWidth: true
                        }
                    }
                }
            }	// Column
        }
    }
}
