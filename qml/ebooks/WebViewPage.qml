
import QtQuick 2.9
import QtQuick.Controls 1.4
import QtWebView 1.1
import "../navigation"

StackPage {
    id: rootPage
    title: "Stáhnout e-knihu"
    property string downloadLink
    signal zipLinkReceived(string zipLink)

	titleBarMenuContent: Row {
		width: refreshButton.width * 2
		anchors.top: parent.top
		anchors.bottom: parent.bottom
		TitleBarButton {
			id: refreshButton
			Image {
				anchors.centerIn: parent
				height:  parent.height / 2
				width: parent.height / 2
				source: "qrc:/resources/refresh.png"
				fillMode: Image.PreserveAspectFit
				clip: true
			}
			onClicked: {
				webView.url = ""
				webView.url = rootPage.downloadLink
			}
		}
		TitleBarButton {
			Image {
				anchors.centerIn: parent
				height:  parent.height / 2
				width: parent.height / 2
				source: "qrc:/resources/external_link.png"
				fillMode: Image.PreserveAspectFit
				clip: true
			}
			onClicked: {
				webView.stop()
				rootPage.close()
				Qt.openUrlExternally(rootPage.downloadLink)
			}
		}
	}

    WebView {
        id: webView
        anchors.fill: parent
        anchors.margins: 1 * mm
        url: rootPage.downloadLink
        onUrlChanged: {
            var urlStr = url.toString()
            console.log("URL: " + urlStr)
            if(urlStr.substr(urlStr.length - 4) === ".zip") {
                webView.stop()
                rootPage.zipLinkReceived(urlStr)
            }
        }
    }
}
