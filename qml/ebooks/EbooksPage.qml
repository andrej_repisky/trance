
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import custom 1.0
import "../navigation"
import "../controls"

Tab {
    id: rootTab
    title: "E-knihy"
    Item {
        id: rootItem
        EbooksTitleBar {
            id: titleBar
        }

        Item {
            anchors.top: titleBar.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            ListView {
                property variant booksModel: {[]}
                id: listView
                anchors.fill: parent
                model: booksModel
                clip: true
                maximumFlickVelocity: 9999
                onBooksModelChanged: {
                    _mainObject.clearImageCache()
                }
                delegate: Rectangle {
                    id: delegateItem
                    color: "white"
                    height: 14 * mm
                    width: listView.width
                    clip: true
                    PaintedImage {
                        id: image
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.leftMargin: mm
                        width: height * 0.666
                        source: modelData.publicationId
                    }
                    Item {
                        id: textItem
                        anchors.left: image.right
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.margins: mm
                        clip: true
                        Label {
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.top: parent.top
                            text: modelData.title
                            font.bold: true
                            textFormat: Text.StyledText
                            elide: Text.ElideRight
                        }
                        Label {
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            text: modelData.authors
                            textFormat: Text.StyledText
                        }
                        Label {
                            id: detailsLabel
                            anchors.left: parent.left
                            anchors.right: formatLabel.left
                            anchors.bottom: parent.bottom
                            anchors.rightMargin: mm
                            text: modelData.language + "/" + modelData.releaseYear + "/" + modelData.genres
                            elide: Text.ElideRight
                        }
                        Label {
                            id: formatLabel
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            text: modelData.format
                            font.bold: true
                        }
                    }
                    Rectangle {
                        anchors.fill: parent
                        color: "black"
                        opacity: 0.1
                        visible: mouseArea.pressed
                    }
                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        onClicked: {
                            var page = detailPageComponent.createObject()
                            page.ebookData = listView.booksModel[index]
                            stackView.push(page)
                        }
                    }
                    CSeparator {
                        anchors.bottom: parent.bottom
                    }
                }	// delegate item
                Component {
                    id: detailPageComponent
                    DetailPage {
                    }
                }
                CScrollBar {
                }
            }	// ListView
            EmptyIndicator {
                id: instructionLabel
                text: "Zadej název publikace nebo jméno autora do horní lišty."
            }
            EmptyIndicator {
                id: noHitsLabel
                visible: listView.count === 0 &&
                         !serverComm.active &&
                         !instructionLabel.visible
            }
            Rectangle {
                visible: titleBar.searchOpen
                anchors.fill: parent
                color: "#50000000"
                MouseArea {
                    anchors.fill: parent
                }
                HistoryItem {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.leftMargin: 7 * mm
                    anchors.rightMargin: 7 *mm
                    anchors.topMargin: 11 * mm
                    onEntrySelected: {
                        titleBar.searchText = text
                        instructionLabel.visible = false
                        titleBar.lastSearch = titleBar.searchText
                        titleBar.searchOpen = false
                        listView.booksModel = JSON.parse(networkCache.getValue(text))
                    }
                    model: networkCache.model
                }
            }
        }	// content item
        CServerCommunicator {
            id: serverComm
            onResponseReceived: {
                instructionLabel.visible = false
                titleBar.lastSearch = titleBar.searchText
                listView.booksModel = data
                networkCache.insert(titleBar.searchText, JSON.stringify(data))
            }
            onActiveChanged:
                spinner.visible = active
        }
        NetworkCache {
            id: networkCache
        }
    }	// Rectangle
}

