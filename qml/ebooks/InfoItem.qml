
import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
	anchors.left: parent.left
	anchors.right: parent.right
	property alias caption: captionLabel.text
	property alias text: contentLabel.text
	height: childrenRect.height
	clip: true
	Label {
		id: captionLabel
		anchors.top: parent.top
		anchors.left: parent.left
		anchors.right: parent.right
		color: "grey"
	}
	Label {
		id: contentLabel
		anchors.top: captionLabel.bottom
		anchors.left: parent.left
		anchors.right: parent.right
		wrapMode: Text.WordWrap
        textFormat: Text.StyledText
	}
}
