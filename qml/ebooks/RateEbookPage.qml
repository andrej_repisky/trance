
import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import custom 1.0
import "../navigation"
import "../controls"

StackPage {
    id: rootPage
    property variant ebookData
    onEbookDataChanged: {
        title = "Ohodnotit „" + ebookData.title + "“"
    }
    Flickable {
        id: flickable
        anchors.fill: parent
        topMargin: 2 * mm
        bottomMargin: 2 * mm
        clip: true
        flickableDirection: Flickable.VerticalFlick
        contentHeight: rootItem.height
        Item {
            id: rootItem
            anchors.left: parent.left
            anchors.right: parent.right
            property real margins: 3 * mm
            anchors.margins: margins
            height: childrenRect.height
            Column {
                id: mainColumn
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 2 * mm
                ListModel {
                    id: rateModel
                    ListElement { text: "Nehodnotit"; value: -1 }
                    ListElement { text: "0 %"; value: 0 }
                    ListElement { text: "10 %"; value: 10 }
                    ListElement { text: "20 %"; value: 20 }
                    ListElement { text: "30 %"; value: 30 }
                    ListElement { text: "40 %"; value: 40 }
                    ListElement { text: "50 %"; value: 50 }
                    ListElement { text: "60 %"; value: 60 }
                    ListElement { text: "70 %"; value: 70 }
                    ListElement { text: "80 %"; value: 80 }
                    ListElement { text: "90 %"; value: 90 }
                    ListElement { text: "100 %"; value: 100 }
                }
                BiggerLabel {
                    text: "Hodnocení e-knihy:"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                ComboBox {
                    id: ebookRatingCombo
                    anchors.horizontalCenter: parent.horizontalCenter
                    model: rateModel
                    width: appWindow.width * 0.43
                    onCurrentIndexChanged: console.debug(rateModel.get(currentIndex).text + ", " + rateModel.get(currentIndex).value)
                }
                BiggerLabel {
                    text: "Komentář k e-knize:"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                TextArea {
                    id: ebookCommentArea
                    anchors.left: parent.left
                    anchors.right: parent.right
                }
                CSeparator { }
                BiggerLabel {
                    text: "Hodnocení publikace:"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                ComboBox {
                    id: pubRatingCombo
                    anchors.horizontalCenter: parent.horizontalCenter
                    model: rateModel
                    width: appWindow.width * 0.43
                    onCurrentIndexChanged: console.debug(rateModel.get(currentIndex).text + ", " + rateModel.get(currentIndex).value)
                }
                BiggerLabel {
                    text: "Komentář k publikaci:"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                TextArea {
                    id: pubCommentArea
                    anchors.left: parent.left
                    anchors.right: parent.right
                }
                CSeparator { }
                Button {
                    id: button
                    text: "Odeslat"
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: {
                        confirmMessage = ""
                        var ebookRating = ebookRatingCombo.model.get(ebookRatingCombo.currentIndex).value
                        if(ebookRating !== -1) {
                            var outData0 = {
                                "itemId": rootPage.ebookData.id,
                                "rating": ebookRating
                            }
                            commEbookRating.send("setEbookRating", outData0)
                        }
                        var ebookComment = ebookCommentArea.text
                        if(ebookComment != "") {
                            var outData1 = {
                                "itemId": rootPage.ebookData.id,
                                "text": ebookComment
                            }
                            commEbookComment.send("addEbookComment", outData1)
                        }
                        var pubRating = pubRatingCombo.model.get(pubRatingCombo.currentIndex).value
                        if(pubRating !== -1) {
                            var outData2 = {
                                "itemId": rootPage.ebookData.publicationId,
                                "rating": pubRating
                            }
                            commPubRating.send("setPublicationRating", outData2)
                        }
                        var pubComment = pubCommentArea.text
                        if(pubComment != "") {
                            var outData3 = {
                                "itemId": rootPage.ebookData.publicationId,
                                "text": pubComment
                            }
                            commPubComment.send("addPublicationComment", outData3)
                        }
                    }
                    property string confirmMessage
                    CServerCommunicator {
                        id: commEbookRating
                        onResponseReceived: {
                            if(data["result"] === "OK") {
                                button.confirmMessage += "Hodnocení eknihy bylo uloženo. Celkové hodnocení: "
                                        + data["overallRating"] + " \%\n"
                            }
                        }
                        onActiveChanged:
                            button.updateSpinner()
                    }
                    CServerCommunicator {
                        id: commEbookComment
                        onActiveChanged:
                            button.updateSpinner()
                    }
                    CServerCommunicator {
                        id: commPubRating
                        onResponseReceived: {
                            if(data["result"] === "OK") {
                                button.confirmMessage += "Hodnocení publikace bylo uloženo. Celkové hodnocení: "
                                        + data["overallRating"] + " \%\n"
                            }
                        }
                        onActiveChanged:
                            button.updateSpinner()
                    }
                    CServerCommunicator {
                        id: commPubComment
                        onActiveChanged:
                            button.updateSpinner()
                    }
                    function updateSpinner() {
                        var oldValue = spinner.visible
                        spinner.visible = commEbookRating.active || commEbookComment.active || commPubComment.active || commPubRating.active
                        if(oldValue == true && spinner.visible == false) {
                            rootPage.close()
                            showToastTimer.start()
                        }
                    }
                    Timer {
                        id: showToastTimer
                        interval: 1
                        onTriggered:
                            _mainObject.showToast(button.confirmMessage)
                    }
                } // Button
            } // Column
        } // Item
    } // Flickable
}
