

import QtQuick 2.4
import "../controls"

Rectangle {
    id: rootItem
    property alias model: listView.model
    visible: listView.count > 0
    color: "#d0ffffff"
    radius: 2 * mm
    clip: true
    height: parent.height * 0.5
    signal entrySelected(var text)
    BiggerLabel {
        id: title
        anchors.top: parent.top
        anchors.margins: 2 * mm
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Uložená hledání"
    }
    ListView {
        id: listView
        anchors.top: title.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: rootItem.radius
        clip: true
        maximumFlickVelocity: 9999

        delegate: Rectangle {
            id: delegateItem
            color: "transparent"
            height: 9 * mm
            width: listView.width
            CSeparator {
                anchors.top: parent.top
                visible: index === 0
            }
            BiggerLabel {
                anchors.centerIn: parent
                text: modelData
            }
            Rectangle {
                anchors.fill: parent
                color: "black"
                opacity: 0.1
                visible: mouseArea.pressed
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: {
                    rootItem.entrySelected(modelData)
                }
            }
            CSeparator {
                anchors.bottom: parent.bottom
            }
        }
        CScrollBar {
        }
    }
}
