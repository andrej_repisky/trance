import QtQuick 2.2

Rectangle {
    id: listScroll
	width: 0.7 * mm
    height: {
        var t = parent.height * parent.height / parent.contentHeight
        if(t < 20) return 20
        return t
    }
    z: 1
    y: 0
	anchors.right: parent.right
	anchors.rightMargin: 0.3 * mm
	color: "#40000000"
	radius: width
    visible: parent.contentHeight > parent.height

	MouseArea {
		id: indicatorArea
		enabled: false
		anchors.fill: parent
		drag.axis: Drag.YAxis
		drag.target: parent
		drag.minimumY: 0
		drag.maximumY: listScroll.parent.height - height
	}

    onYChanged: {
        if(indicatorArea.drag.active) {
            var t = y - parent.y
            t = (parent.contentHeight - parent.height) * (t / (parent.height - height))
            parent.contentY = t
        }
    }
    Connections {
        target: parent
        onContentYChanged: {
            if(!indicatorArea.drag.active) {
                var t = parent.height - listScroll.height
                listScroll.y = (parent.contentY / (parent.contentHeight - parent.height)) * t
            }
        }
    }
}
