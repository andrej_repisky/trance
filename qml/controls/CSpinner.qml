
import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
	anchors.fill: parent
	Rectangle {
		anchors.fill: parent
		color: "#70ffffff"
	}
	BusyIndicator {
		anchors.centerIn: parent
	}
	MouseArea {
		anchors.fill: parent
	}
	z: 10
}

