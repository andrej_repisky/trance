
import QtQuick 2.4
import QtQuick.Controls 1.3

Item {
	anchors.fill: parent
	property alias text: label.text
	Rectangle {
		color: "#B0000000"
		radius: 0.6 * mm
		anchors.verticalCenter: parent.verticalCenter
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.margins: 3 * mm
		height: label.height + 6 * mm
		Label {
			id: label
			anchors.verticalCenter: parent.verticalCenter
			anchors.left: parent.left
			anchors.right: parent.right
			anchors.margins: 1.5 * mm
			color: "white"
			horizontalAlignment: Text.AlignHCenter
			wrapMode: Text.WordWrap
		}
	}
}
