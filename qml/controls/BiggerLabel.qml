
import QtQuick 2.5
import QtQuick.Controls 1.4

Label {
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 3 * mm
}
