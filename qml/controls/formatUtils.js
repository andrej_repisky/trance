
function formatSize(value) {
    var retVal = value < 100000 ?
		(Math.round(value * 0.01) / 10 + " kB") :
		(Math.round(value * 0.00001) / 10 + " MB");
    return retVal.replace(".", ",")
}
