
import QtQuick 2.4
import QtQuick.Controls 1.3

Label {
	anchors.left: parent.left
	anchors.right: parent.right
	anchors.verticalCenter: parent.verticalCenter
	anchors.margins: 2 * mm
	text: "Žádné výsledky"
	color: "grey"
	font.pixelSize: 3 * mm
	font.italic: true
	horizontalAlignment: Text.AlignHCenter
	wrapMode: Text.WordWrap
}
