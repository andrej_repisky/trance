import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import custom 1.0
import ".."
import "../ebooks"
import "../settings"

TabView {
	id: tabView
    currentIndex: 0
	EbooksPage {
	}
	DownloadsPage {
		id: dlnPage
	}
	SettingsPage {
		active: true
	}
	HelpPage {
	}
    onCurrentIndexChanged: {
        if(tabView.getTab(tabView.currentIndex) !== dlnPage) {
            dlnPage.active = false
        }
    }
	style: TabViewStyle {
		tab: Item {
			TabEntry {
				title: styleData.title
				selected: styleData.selected
				index: styleData.index
			}
		}
	}
}
