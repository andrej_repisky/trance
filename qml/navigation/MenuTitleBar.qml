import QtQuick 2.4

TitleBar {
	buttonContent: Column {
		anchors.centerIn: parent
		property real rectHeight: 0.4 * mm
		width: 3.5 * mm
		spacing: rectHeight + 0.1 * mm
		Rectangle {
			width: parent.width
			height: parent.rectHeight
			radius: height
			color: "white"
		}
		Rectangle {
			width: parent.width
			height: parent.rectHeight
			radius: height
			color: "white"
		}
		Rectangle {
			width: parent.width
			height: parent.rectHeight
			radius: height
			color: "white"
		}
	}
}
