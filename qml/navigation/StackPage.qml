
import QtQuick 2.4

Rectangle {
	id: rootItem
	default property alias mainContent: mainItem.data
    property alias titleBarMenuContent: titleBar.rightContent
	property alias title: titleBar.text
	signal closed()
	ArrowTitleBar {
		id: titleBar
		onClicked:
			close()
	}
	Item {
		id: mainItem
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.top: titleBar.bottom
        anchors.bottom: parent.bottom
	}
	function close() {
		stackView.pop()
		closed()
		destroyTimer.start()
	}
	Timer {
		id: destroyTimer
		interval: 2000
		repeat: false
		onTriggered: {
			rootItem.destroy()
			console.log("StackPage destroyed")
		}
	}
}
