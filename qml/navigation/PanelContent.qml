import QtQuick 2.4
import QtQuick.Controls 1.3

Rectangle {
	id: root
	anchors.fill: parent
	signal itemClicked(int index)
	ListView {
		id: listView
		anchors.fill: parent
		model: _tabModel
		boundsBehavior: Flickable.StopAtBounds
		delegate: Rectangle {
			height: 7.5 * mm
			width: listView.width
			color: mouseArea.pressed ? "lightgrey" : "transparent"
			Label {
				anchors.centerIn: parent
				text: title
				font.pixelSize: parent.height / 2.67
				font.family: "Roboto"
				font.bold: true
                color: selected ? "#a0a0a0" : _fgColor
			}
			MouseArea {
				id: mouseArea
				anchors.fill: parent
				onClicked:
					root.itemClicked(index)
			}
		}
	}
}

