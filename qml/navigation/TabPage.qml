
import QtQuick 2.4
import QtQuick.Controls 1.3

Tab {
    id: rootTab
    default property Component content
	property string color: "transparent"
	Rectangle {
        id: rootItem
		color: rootTab.color
        MenuTitleBar {
            id: titleBar
            text: rootTab.title
            onClicked:
                drawer.toggle()
        }
        Loader {
            //id: mainItem
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: titleBar.bottom
            anchors.bottom: parent.bottom
            sourceComponent: rootTab.content
        }
    }
}

