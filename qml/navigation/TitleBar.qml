
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Rectangle {
	id: root
	property alias text: label.text
	property alias buttonContent: leftButton.data
	property alias rightContent: rightItem.data
	signal clicked()
    height: 7.3 * mm
	color: _fgColor
	anchors.top: parent.top
	anchors.left: parent.left
	anchors.right: parent.right
	MouseArea {		// prevent taps from propagating deeper
		anchors.fill: parent
	}
	RowLayout {
		anchors.fill: parent
		spacing: 1 * mm
		TitleBarButton {
			id: leftButton
			onClicked: root.clicked()
		}
		Label {
			id: label
//			anchors.verticalCenter: parent.verticalCenter
            Layout.alignment: Qt.AlignVCenter
			color: "white"
			//font.family: "Droid Sans Fallback"
			font.family: "Roboto"
			font.bold: true
			font.pixelSize: root.height / 2.2
			elide: Text.ElideRight
			Layout.fillWidth: true
		}
		Item {
            id: rightItem
            height: parent.height
			width: childrenRect.width
		}
	}
}

