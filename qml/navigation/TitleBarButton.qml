
import QtQuick 2.4

Rectangle {
	id: root
    height: parent.height
	signal clicked()
	width: 10 * mm
	color: mouseArea.pressed ? "#30ffffff" : "transparent"
	MouseArea {
		id: mouseArea
		anchors.fill: parent
		onClicked:
			root.clicked()
	}
}
