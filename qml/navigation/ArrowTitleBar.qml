
import QtQuick 2.4

TitleBar {
	buttonContent: Item {
		anchors.centerIn: parent
		property real rectHeight: 0.4 * mm
		width: 3.5 * mm
		height: width
		Rectangle {
			height: parent.rectHeight
			width: parent.width
			radius: height
			anchors.centerIn: parent
			color: "white"
		}
		Rectangle {
			height: parent.rectHeight
			width: parent.width * 0.6
			radius: height
			anchors.left: parent.left
			anchors.verticalCenter: parent.verticalCenter
			rotation: 45
			transformOrigin: Item.Left
			color: "white"
		}
		Rectangle {
			height: parent.rectHeight
			width: parent.width * 0.6
			radius: height
			anchors.left: parent.left
			anchors.verticalCenter: parent.verticalCenter
			rotation: -45
			transformOrigin: Item.Left
			color: "white"
		}
	}
}
