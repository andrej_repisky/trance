
import QtQuick 2.4
import QtQuick.Controls 1.3
import custom 1.0
import "../navigation"
import "../controls"

TabPage {
    title: "Nastavení"
    Flickable {
        id: flickable
        anchors.fill: parent
        topMargin: 2 * mm
        bottomMargin: 2 * mm
        clip: true
        flickableDirection: Flickable.VerticalFlick
        contentHeight: rootItem.height
        Item {
            id: rootItem
            anchors.left: parent.left
            anchors.right: parent.right
            property real margins: 3 * mm
            anchors.margins: margins
            height: childrenRect.height
            Column {
                id: mainColumn
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 2 * mm
                Component.onCompleted: {
                    var username = _settings.value("username", "")
                    var password = _settings.value("password", "")
                    if(username === "" || password === "")
                        return
                    serverCommLogin.setUsernamePassword(username, password)
                }

                BiggerLabel {
                    text: "Přihlášení"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Column {
                    spacing: parent.spacing
                    anchors.left: parent.left
                    anchors.right: parent.right
                    visible: !serverCommLogin.signedIn
                    TextField {
                        id: nameField
                        anchors.left: parent.left
                        anchors.right: parent.right
                        placeholderText: "Uživatelské jméno"
                        onAccepted:
                            pwdField.focus = true
                    }
                    TextField {
                        id: pwdField
                        anchors.left: parent.left
                        anchors.right: parent.right
                        placeholderText: "Heslo"
                        echoMode: TextInput.Password
                        onAccepted:
                            signInButton.run()
                    }
                    Button {
                        id: signInButton
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Přihlásit se"
                        onClicked:
                            run()
                        function run() {
                            nameField.focus = false
                            pwdField.focus = false
                            serverCommLogin.setUsernamePassword(nameField.text, pwdField.text)
                            var dummyRequest = { "itemCount": 1 }
                            serverCommLogin.send("getEbookList", dummyRequest)
                        }
                    }
                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: "V případě, že nemáš uživatelský účet, otevři si http://xtrance.info na svém PC a postupuj podle instrukcí."
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                        color: "grey"
                    }
                }	// sign-in Column
                Column {
                    spacing: parent.spacing
                    anchors.left: parent.left
                    anchors.right: parent.right
                    visible: serverCommLogin.signedIn
                    Label {
                        id: userLabel
                        anchors.left: parent.left
                        anchors.right: parent.right
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                    }
                    Button {
                        text: "Odhlásit se"
                        anchors.horizontalCenter: parent.horizontalCenter
                        onClicked: {
                            serverCommLogin.setUsernamePassword("", "")
                        }
                    }
                    Connections {
                        target: serverCommLogin
                        onSignedInChanged: {
                            if(serverCommLogin.signedIn && nameField.text != "" && pwdField.text != "") {
                                _settings.setValue("username", nameField.text)
                                _settings.setValue("password", pwdField.text)
                            }
                            if(serverCommLogin.signedIn)
                                userLabel.text = "Jsi přihlášený jako „" + _settings.value("username") + "“."
                            else {
                                _settings.setValue("username", "")
                                _settings.setValue("password", "")
                            }
                        }
                    }

                }	// sign-out column
                CSeparator {
                    visible: !_isIOS
                    anchors.leftMargin: -1 * rootItem.margins
                    anchors.rightMargin: -1 * rootItem.margins
                }
                Column {
                    visible: !_isIOS
                    spacing: parent.spacing
                    anchors.left: parent.left
                    anchors.right: parent.right
                    BiggerLabel {
                        text: "Adresář se .zip soubory:"
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    TextField {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        Component.onCompleted: {
                            text = _settings.downloadPath()
                        }
                        onTextChanged: {
                            _settings.setValue("downloadPath", text)
                        }
                    }
                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: "Zadej cestu, do které tvůj prohlížeč ukládá stažené soubory. " +
                              "Rozbalené eknihy budou uloženy do podadresáře „trance_eknihy“."
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WordWrap
                        color: "grey"
                    }
                }
            }	// Column
        }	// Item
    }	// Flickable
    color: "white"
}
