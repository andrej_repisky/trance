
import QtQuick 2.9
import QtQuick.Controls 1.4

import "navigation"

TabPage {
	title: "Nápověda" 
	Item {
		anchors.fill: parent
        anchors.leftMargin: 2 * mm
        anchors.rightMargin: 2 * mm
        Flickable {
            id: flickable
            anchors.fill: parent
            topMargin: 2 * mm
            bottomMargin: 2 * mm
            clip: true
            flickableDirection: Flickable.VerticalFlick
            contentHeight: topColumn.height
            Column {
                id: topColumn
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: mm
                clip: true
                Label {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: "<center><h4>Stažení knihy – jednodušší způsob</h4></center>
                Funguje pouze s úložištěm Zippyshare. Odkazy na toto úložiště se zobrazují pouze pokud je tvoje úroveň práv „běžný uživatel“ a vyšší.
                <ol type=\"1\">
                <li>Najdi hledanou e-knihu podle názvu nebo jména autora. Otevři detajl knihy.</li>
                <li>Klikni na tlačítko stažení (Server č. 1). Odkaz na stahovací server se otevře v aplikaci.</li>
                <li>Klikni na tlačítko „Download now“ (úplně vpravo). Stránka zmizí a aplikace skočí na Stažené soubory, kde bude nahoře nově stažená kniha.</li>
                <li>Kliknutím na Otevřít se kniha otevře ve výchozí aplikaci, kterou je nutné mít instalovanou na mobilu. Nejlíp to funguje s formáty .epub a .mobi a s aplikací <a href=\"https://play.google.com/store/apps/details?id=org.geometerplus.zlibrary.ui.android\">FBReader</a>. </li>
                </ol>
                <br>
                <center><h4>Složitější a obecnější způsob</h4></center>
                <p>Stahovací odkazy,  které nevedou na Zippyshare, se otevírají v externím prohlížeči. Po stažení souboru je nutné se ručně vrátit do aplikace a otevřít Stažené soubory.</p>
                <br>
                <p>Zpětnou vazbu k aplikaci piš do <a href=\"http://ebookforum.sk/viewtopic.php?f=15&t=8647\">příslušného vlákna na ebookforum.sk</a>.</p>
                <p>Tato aplikace je svobodný softvér. Na používání a šíření se vztahuje Veřejná licence Evropské unie. </p>
                <p>Ikona aplikace byla stažena z <a href=\"http://www.softicons.com/toolbar-icons/ravenna-3d-icons-by-double-j-design/books-icon\">www.softicons.com</a>. </p>
                <p>Datum sestavy: " + _mainObject.buildDate() + "</p>"
                    textFormat: Text.RichText
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
        }
	}
	color: "white"
}

