
import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import custom 1.0
import "navigation"
import "controls"

ApplicationWindow {
	id: appWindow
	title: qsTr("Trance")
    width: 460
    height: 700
    visible: true

	StackView {
		id: stackView
		anchors.fill: parent
		focus: true
		initialItem:
			MainTabView {
				id: tabView
			}
        Keys.onReleased: {
            if(event.key !== Qt.Key_Back && event.key !== Qt.Key_Escape)
                return

            event.accepted = true
            if(stackView.depth > 1) {
                stackView.currentItem.close()
                return
            }
            if(tabView.currentIndex !== 0) {
                tabView.currentIndex = 0
                return
            }
            if(backButtonTimer.running === false) {
                _mainObject.showToast("Pro ukončení zmáčkni tlačítko Zpět dvakrát.")
                backButtonTimer.start()
            } else
                Qt.quit()
		}
        Timer {
            id: backButtonTimer
            interval: 500
            repeat: false
        }
	}

	NavigationDrawer {
		id: drawer
		visible: (stackView.depth == 1) && !spinner.visible
		anchors.top: parent.top
		anchors.topMargin: 7.3 * mm	//titleBar.height
		anchors.bottom: parent.bottom
		position: Qt.LeftEdge
		visualParent: parent
		PanelContent {
			onItemClicked: {
				tabView.currentIndex = index
				drawer.open = false
			}
		}
	}

	CSpinner {
		id: spinner
		visible: false
	}

	CServerCommunicator {
		id: serverCommLogin
		onActiveChanged:
			spinner.visible = active
	}

}
